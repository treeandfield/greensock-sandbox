$(document).ready(function(){
	
	// load the first example
	$('nav ul li:first').addClass('on');
	var f = $('nav ul li:first').html().toLowerCase();
	
	//var f ='seven'; // working
	
	$( "#example" ).load( "_inc/html/"+f+".html?cb=" +Math.random());
	
	// nav action
	$('nav ul li').bind('click', function(){
		
		$('nav ul li').removeClass('on');
		$(this).addClass('on');
		
		var p = $(this).html().toLowerCase();
		$( "#example" ).load( "_inc/html/"+p+".html?cb="+Math.random(), function() {
		  	console.log(p + " loaded.");	
		});

	});
	
});